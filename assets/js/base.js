jQuery(document).ready(function() {

	//------------------------------------------------------------------------
	//	Basket checkboxes
	//------------------------------------------------------------------------	

	$('#newAddress').click(function() {
		$('input[name="productInfo"]').prop('disabled', false);
	});

	$('#address').click(function() {
		$('input[name="productInfo"]').prop('disabled', true);
	});

	//------------------------------------------------------------------------
	//	Hide Collapse Menu on mobile
	//------------------------------------------------------------------------	
	
	$(document).ready(allCollapse);
	$(window).on('resize',allCollapse);

	function allCollapse() {
		if ($(this).width() <= 991) {
			$('.user-menu__collapse').removeClass('in');
			$('.b-filter__collapse').removeClass('in');		
		} else {
			$('.user-menu__collapse').addClass('in');
			$('.b-filter__collapse').addClass('in');			
		}
	}
	
	//------------------------------------------------------------------------
	//	jQuery Bar Rating http://antenna.io/demo/jquery-bar-rating/examples/
	//------------------------------------------------------------------------ 

	$('.js-rate-stars').barrating({
		theme: 'fontawesome-stars'
	});	

	//------------------------------------------------------------------------
	//	Product Carousel
	//------------------------------------------------------------------------ 	
	
	var productCarousel = $('.b-product-carousel');

	productCarousel.owlCarousel({		
		autoplay: false,
		autoplayTimeout: 3000,
		smartSpeed: 1100,
		autoplayHoverPause:true,
		loop: true,
		center: false,
		margin: 7,	
		mouseDrag: true,		
		dots: false, 	
		navText: false,
		nav: true,
		responsive : {
			 // breakpoint from 0 up
			 0 : {
						
			 },
			 // breakpoint from 320 up
			 320 : {
				items: 1,							
			 },			 
			 // breakpoint from 480 up
			 480 : {
				items: 1,					
			 },
			 // breakpoint from 768 up
			 768 : {
				items: 3,						
			 },
			 // breakpoint from 992 up
			 992 : {
				items: 4,
			 },					 
		}		
	});

	//------------------------------------------------------------------------
	//	Product Category
	//------------------------------------------------------------------------ 	
	
	var categoryCarousel = $('.b-category-carousel');

	categoryCarousel.owlCarousel({		
		autoplay: false,
		autoplayTimeout: 3000,
		smartSpeed: 1100,
		autoplayHoverPause:true,
		loop: false,
		center: false,
		margin: 15,	
		mouseDrag: true,		
		dots: false, 	
		navText: false,
		nav: true,
		responsive : {
			 // breakpoint from 0 up
			 0 : {
						
			 },
			 // breakpoint from 320 up
			 320 : {
				items: 3,							
			 },			 
			 // breakpoint from 480 up
			 480 : {
				items: 4,					
			 },
			 // breakpoint from 768 up
			 768 : {
				items: 6,						
			 },
			 // breakpoint from 992 up
			 992 : {
				items: 8,
			 },					 
		}		
	});
			
	//------------------------------------------------------------------------
	// Close opened .modal on opening new modal
	// you can give custom class like this 
	// var modalUniqueClass = ".modal.modal-unique";
	//------------------------------------------------------------------------ 

	var modalUniqueClass = ".modal";
	$('.modal').on('show.bs.modal', function(e) {
	  var $element = $(this);
	  var $uniques = $(modalUniqueClass + ':visible').not($(this));
	  if ($uniques.length) {
		$uniques.modal('hide');
		$uniques.one('hidden.bs.modal', function(e) {
		  $element.modal('show');
		});
		return false;
	  }
	});
	
	//------------------------------------------------------------------------
	//	Always active only one bootstrap dropdown
	//------------------------------------------------------------------------ 

	$('.navbar').on('show.bs.collapse', function () {
		var actives = $(this).find('.collapse.in'),
			hasData;
		
		if (actives && actives.length) {
			hasData = actives.data('collapse')
			if (hasData && hasData.transitioning) return
			actives.collapse('hide')
			hasData || actives.data('collapse', null)
		}
	});

	//------------------------------------------------------------------------
	//	Bootstrap Show Password https://github.com/wenzhixin/bootstrap-show-password
	//------------------------------------------------------------------------	

	$(".password").password({
		eyeClass: 'fa',
		eyeOpenClass: 'fa-eye',
		eyeCloseClass: 'fa-eye-slash'
	})	
	
	//------------------------------------------------------------------------
	//	Custom background color with data attribute
	//------------------------------------------------------------------------ 	
	
	$("[data-bg-color]").css('background', function () {
		return $(this).data('bg-color')
	});
	
	//------------------------------------------------------------------------
	//	Custom color with data attribute
	//------------------------------------------------------------------------ 	
	
	$("[data-color]").css('color', function () {
		return $(this).data('color')
	});

	//------------------------------------------------------------------------
	//	Bootstrap Selecpicker
	//------------------------------------------------------------------------ 	

	$('.selectpicker').selectpicker({
		size: 4
	});

	//------------------------------------------------------------------------
	//	Bootstrap Tootltip
	//------------------------------------------------------------------------ 	
	
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})	

	//------------------------------------------------------------------------
	//	Bootstrap Datepicker
	//------------------------------------------------------------------------
	
	$('.datepicker').datepicker({
		/*language: 'ru',*/
		format: "dd MM yyyy",
		ignoreReadonly: true,	
	});	
	
	//------------------------------------------------------------------------
	//	Bootstrap Popover
	//------------------------------------------------------------------------
		
	$('[data-toggle="popover"]').popover({
		trigger: "focus",
		container: "body",   
	}); 	
		
});