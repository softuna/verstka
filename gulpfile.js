var path = require('path');
var gulp = require('gulp');
var noop = require('gulp-noop');
var cleanCss = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var htmlreplace = require('gulp-html-replace');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');
var rev = require('gulp-rev');
var fingerprint = require('gulp-fingerprint');
var del = require('del');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var merge = require('merge-stream');
var OUT_CSS_NAME = 'css/all.min.css';
var VENDOR_OUT_JS_NAME = 'js/vendor.min.js';
var SITE_OUT_JS_NAME = 'js/scripts.min.js';
var SCSS_ROOT = 'main.scss';
var CSS_LOCATIONS = [SCSS_ROOT, 'base/**/*.css', 'components/**/*.css', 'pages/**/*.css', 'vendors/**/*.css', '**/*.scss'];
var VENDOR_JS = ['vendors/**/*.js', '!**/*.min.js'];
var SITE_JS = ['assets/js/**/*.js', 'components/**/*.js', '!**/*.min.js']
var JS_LOCATIONS = [].concat(VENDOR_JS, SITE_JS);
var ASSETS_LOCATIONS = ['assets/fonts/**/*', 'assets/img/**/*'];
var COMPONENTS_IMAGES = ['components/**/*.png', 'components/**/*.jpg', 'components/**/*.svg', 'components/**/*.gif'];
var PAGES_IMAGES = ['pages/**/*.png', 'pages/**/*.jpg', 'pages/**/*.svg', 'pages/**/*.gif'];
var HTML_LOCATIONS = ['dev/**/*.html'];
var VENDOR_ASSETS = ['vendors/**/fonts/**', 'vendors/*/img/**'];
var prod = false;

gulp.task('clean', function() {
    return del(['dist', 'temp']);
});

gulp.task('build-js', ['clean'], function() {
    var vendor = gulp.src(VENDOR_JS)
        .pipe(concat(VENDOR_OUT_JS_NAME))
        .pipe(prod ? uglify() : noop())
        .pipe(gulp.dest('temp'));
    var site = gulp.src(SITE_JS)
        .pipe(concat(SITE_OUT_JS_NAME))
        .pipe(prod ? uglify() : noop())
		.on('error', function (err) { console.log(err.toString()); })
        .pipe(gulp.dest('temp'));
	return merge(vendor, site);
});

gulp.task('build-css', ['clean'], function() {
    return gulp.src([SCSS_ROOT])
	    .pipe(sass().on('error', sass.logError))
        .pipe(concat(OUT_CSS_NAME))
        .pipe(prod ? cleanCss() : noop())
        .pipe(gulp.dest('temp'));
});

gulp.task('asset-rev', ['build-js', 'build-css'], function(cb) {
    return gulp.src(['temp/**/*.js', 'temp/**/*.css'])
        .pipe(rev())
        .pipe(gulp.dest('dist'))
        .pipe(rev.manifest())
        .pipe(gulp.dest('dist'));
});

gulp.task('build-html', ['clean'], function() {
	return gulp.src(HTML_LOCATIONS)
		.pipe(gulp.dest('dist'));
});

gulp.task('assets', ['clean'], function() {
	var assets = gulp.src(ASSETS_LOCATIONS, {base: "assets"})
		.pipe(gulp.dest('dist'));
	var components = gulp.src(COMPONENTS_IMAGES)
		.pipe(gulp.dest('dist/img/components'));
	var pages = gulp.src(PAGES_IMAGES)
		.pipe(gulp.dest('dist/img/pages'));
	return merge(assets, components, pages);
});

gulp.task('vendor', ['clean'], function() {
	return gulp.src(VENDOR_ASSETS, {base: 'vendors'})
		.pipe(rename(function(filePath) {
			var parts = filePath.dirname.split(path.sep);
			var part;
			while(parts.length) {
				part = parts[0];
				if ((part === 'fonts') || (part === 'img')) {
					break;
				}
				parts.splice(0, 1);
			}
			filePath.dirname = parts.join(path.sep);
		}))
		.pipe(gulp.dest('dist'));		
});

gulp.task('build', ['assets', 'build-html', 'build-js', 'build-css', 'vendor'], function() {
    return gulp.src(['temp/**/*.js', 'temp/**/*.css'])
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['build'], function() {
	var list = [].concat(JS_LOCATIONS, CSS_LOCATIONS, HTML_LOCATIONS, ASSETS_LOCATIONS, COMPONENTS_IMAGES, PAGES_IMAGES, VENDOR_ASSETS);
	//console.log('watching ', list);
	return watch(list, function() { 
		gulp.run(['build']); 
	});
});

gulp.task('default', ['clean', 'build']);